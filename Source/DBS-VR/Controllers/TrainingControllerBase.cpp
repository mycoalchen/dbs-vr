// Fill out your copyright notice in the Description page of Project Settings.


#include "TrainingControllerBase.h"
#include "Components/CapsuleComponent.h"
#include "../Pitches/PitchBase.h"

// Sets default values
ATrainingControllerBase::ATrainingControllerBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATrainingControllerBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATrainingControllerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATrainingControllerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

void ATrainingControllerBase::OnPitchThrown(APitchBase* Pitch)
{
	ActivePitch = Pitch;
}

void ATrainingControllerBase::DeactivatePitch()
{
	if (!ActivePitch)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("DeactivatePitch called when ActivePitch was null!"));
		return;
	}
}
