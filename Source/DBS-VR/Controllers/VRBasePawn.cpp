// Fill out your copyright notice in the Description page of Project Settings.


#include "VRBasePawn.h"
#include "Camera/CameraComponent.h"

// Sets default values
AVRBasePawn::AVRBasePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VRRoot = CreateDefaultSubobject<USceneComponent>(TEXT("VR_Root"));
	RootComponent = VRRoot;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(VRRoot);
}

// Called when the game starts or when spawned
void AVRBasePawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AVRBasePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AVRBasePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction(FName("GripLeftHand"), IE_Pressed, this, &AVRBasePawn::GripLeftHandPressed);
	InputComponent->BindAction(FName("GripRightHand"), IE_Pressed, this, &AVRBasePawn::GripRightHandPressed);
	InputComponent->BindAction(FName("GripLeftHand"), IE_Released, this, &AVRBasePawn::GripLeftHandReleased);
	InputComponent->BindAction(FName("GripRightHand"), IE_Released, this, &AVRBasePawn::GripRightHandReleased);
}

void AVRBasePawn::GripLeftHandPressed()
{
	UE_LOG(LogTemp, Log, TEXT("Left Hand Grip Pressed"));
}
void AVRBasePawn::GripRightHandPressed()
{
	UE_LOG(LogTemp, Log, TEXT("Right Hand Grip Pressed"));
}
void AVRBasePawn::GripLeftHandReleased()
{
	UE_LOG(LogTemp, Log, TEXT("Left Hand Grip Released"));
}
void AVRBasePawn::GripRightHandReleased()
{
	UE_LOG(LogTemp, Log, TEXT("Left Hand Grip Released"));
}