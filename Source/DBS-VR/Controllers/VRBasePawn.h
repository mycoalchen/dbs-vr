// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "VRBasePawn.generated.h"

UCLASS()
class DBS_VR_API AVRBasePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVRBasePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* VRRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		class UCameraComponent* CameraComponent;

	// Oculus Touch Controls
	UFUNCTION()
		void GripLeftHandPressed();
	UFUNCTION()
		void GripRightHandPressed();
	UFUNCTION()
		void GripLeftHandReleased();
	UFUNCTION()
		void GripRightHandReleased();

};
