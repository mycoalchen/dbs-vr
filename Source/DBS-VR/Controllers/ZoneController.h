// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TrainingControllerBase.h"
#include "ZoneController.generated.h"

/**
 * 
 */
UCLASS()
class DBS_VR_API AZoneController : public ATrainingControllerBase
{
	GENERATED_BODY()

	AZoneController();
	
public:
	virtual void DeactivatePitch() override;
	virtual void OnPitchThrown(class APitchBase* Pitch) override;
	
	UPROPERTY(BlueprintReadWrite, Category = "Swinging")
		bool bCanSwing = true;

	UPROPERTY(BlueprintReadWrite, Category = "View")
		float ZoomSensitivity = 1;
	
	//// Overlay for accuracy, pitch, and swing information
	//UPROPERTY(BlueprintReadWrite, Category = "UI")
	//	class UZoneOverlay* Overlay;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		class TSubclassOf<class UZoneOverlay> OverlayClass;

	// Sets up UI elements; called at beginning
	UFUNCTION()
		void CreateUI();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	// Input handlers
	UFUNCTION()
		void MouseX(float Value);
	UFUNCTION()
		void MouseY(float Value);
	UFUNCTION()
		void LMBDown();
	UFUNCTION()
		void LMBUp();
	bool bLMBHeld = false;
	UFUNCTION()
		void RMBDown();
	UFUNCTION()
		void RMBUp();
	bool bRMBHeld = false;
	int ZoomInOut = 0; // -1 out, 0 none, 1 in
};
