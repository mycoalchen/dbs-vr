// Fill out your copyright notice in the Description page of Project Settings.

#include "CatchingControllerBase.h"
#include "../Pitches/PitchBase.h"
#include "../UI/PrecisionReticle.h"
#include "../UI/PrecisionOverlay.h"
#include "../Pitcher.h"
#include "Blueprint/UserWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/TextBlock.h"
#include "Components/SphereComponent.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/PlayerController.h"
#include "MotionControllerComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/Image.h"

ACatchingControllerBase::ACatchingControllerBase()
{
	SwingSphere = CreateDefaultSubobject<USphereComponent>(FName("SwingSphere"));
	SwingSphere->SetSphereRadius(SwingSphereRadius);

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(FName("MotionController"));
	MotionController->CurrentTrackingStatus = ETrackingStatus::Tracked;
}

void ACatchingControllerBase::BeginPlay()
{
	Super::BeginPlay();
	CreateUI();
	SwingSphere->OnComponentHit.AddDynamic(this, &ACatchingControllerBase::OnSwingSphereHit);
	SwingPlane = FPlane(SwingPlanePoint1, SwingPlanePoint2, SwingPlanePoint3);
	MCOffsetVector = FVector(0, 0, GetActorLocation().Z);
}

void ACatchingControllerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const FVector MCLocation = MotionController->GetComponentLocation();
	SwingSphere->SetWorldLocation(ReticleSensitivity * FVector(0, MCLocation.Y, MCLocation.Z) + MCOffsetVector);
	
	DrawDebugSphere(GetWorld(), SwingSphere->GetComponentLocation(), SwingSphereRadius, 12, FColor::White, false, DeltaTime);

	if (bIsSwinging)
	{
		CurrentSwingDuration += DeltaTime;
		if (CurrentSwingDuration >= SwingDuration)
		{
			bIsSwinging = false;
			CurrentSwingDuration = 0;
			/*Overlay->UpdateHit(false);
			Overlay->UpdateMiss(-SwingToBall, SwingSphereRadius);*/
			OnSwingFinished();
		}
		else
		{
			SwingUpdate();
		}
	}
	// GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::White, BestSwingLocation.ToString());
	// GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Yellow, BestBallLocation.ToString());
}


void ACatchingControllerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(FName("MouseX"), this, &ACatchingControllerBase::MouseX);
	PlayerInputComponent->BindAxis(FName("MouseY"), this, &ACatchingControllerBase::MouseY);
	PlayerInputComponent->BindAction(FName("LMB"), IE_Pressed, this, &ACatchingControllerBase::LeftClick);
}


void ACatchingControllerBase::CreateUI()
{
	if (ReticleClass)
	{
		Reticle = CreateWidget<UPrecisionReticle>(GetWorld(), ReticleClass, FName(TEXT("Reticle")));
		Reticle->AddToViewport();
	}
	if (OverlayClass)
	{
		Overlay = CreateWidget<UPrecisionOverlay>(GetWorld(), OverlayClass, FName(TEXT("Overlay")));
		Overlay->AddToViewport();
	}
}

void ACatchingControllerBase::MouseX(float Value)
{
	//if (Reticle && Reticle->ReticleImage)
	//{
	//	if (UCanvasPanelSlot* ImageSlot = Cast<UCanvasPanelSlot>(Reticle->ReticleImage->Slot)) {
	//		// const float resX = GSystemResolution.ResX; use this instead of Result if buggy on different screens
	//		const float newX = ImageSlot->GetPosition().X + ReticleSensitivity * Value;
	//		FVector2D Result;
	//		GEngine->GameViewport->GetViewportSize(Result);
	//		if (-Result.X * 0.5 < newX - ImageSlot->GetSize().X * 0.5
	//			&& Result.X * 0.5 > newX + ImageSlot->GetSize().X * 0.5)
	//			ImageSlot->SetPosition(FVector2D(newX, ImageSlot->GetPosition().Y));
	//	}
	//}
}

void ACatchingControllerBase::MouseY(float Value)
{
	/*if (Reticle && Reticle->ReticleImage)
	{
		if (UCanvasPanelSlot* ImageSlot = Cast<UCanvasPanelSlot>(Reticle->ReticleImage->Slot))
		{
			const float newY = ImageSlot->GetPosition().Y + ReticleSensitivity * Value;
			FVector2D Result;
			GEngine->GameViewport->GetViewportSize(Result);
			if (-Result.Y * 0.5 < newY - ImageSlot->GetSize().Y * 0.5
				&& Result.Y * 0.5 > newY + ImageSlot->GetSize().Y * 0.5)
				ImageSlot->SetPosition(FVector2D(ImageSlot->GetPosition().X, newY));
		}
	}*/
}

void ACatchingControllerBase::LeftClick()
{
	if (bCanSwing)
	{
		bCanSwing = false;
		if (!ActivePitch) return;

		SwingToBall = FVector(10000, 10000, 10000);

		// Begin the swing
		ActivePitch->Status = EBallStatus::BS_Strike;
		bIsSwinging = true;
		SwingUpdate();
		// Overlay->UpdateSwing(true);
		ReticleSensitivity /= 10;
		SwingSphere->SetCollisionResponseToAllChannels(ECR_Block);
	}
}

void ACatchingControllerBase::OnSwingSphereHit(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	APitchBase* Pitch = Cast<APitchBase>(OtherActor);
	if (Pitch && Pitch == ActivePitch)
	{
		BestBallLocation = Hit.ImpactPoint;
		ActivePitch->Status = EBallStatus::BS_Hit;
		// Overlay->UpdateHit(true);
		OnSwingFinished();
		if (!Pitcher)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("Pitcher null in PrecisionController!"));
			return;
		}
		Pitcher->DeactivatePitch();
	}
}

void ACatchingControllerBase::SwingUpdate()
{
	// GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Blue, TEXT("SwingUpdate called"));

	// Safety first!
	if (!ActivePitch)
	{
		/*Overlay->UpdateHit(false);
		Overlay->UpdateMiss(-SwingToBall, SwingSphereRadius);*/
		OnSwingFinished();
		return;
	}

	// Get reticle and viewport positions
	FVector2D ReticlePosition, ViewportSize;
	FVector WorldPosition, WorldDirection;
	if (Reticle && Reticle->ReticleImage)
	{
		if (UCanvasPanelSlot* ImageSlot = Cast<UCanvasPanelSlot>(Reticle->ReticleImage->Slot))
			ReticlePosition = ImageSlot->GetPosition();
	}
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	GEngine->GameViewport->GetViewportSize(ViewportSize);

	// Move the swing sphere and update the best positions
	if (PC->DeprojectScreenPositionToWorld(ReticlePosition.X + ViewportSize.X * 0.5, ReticlePosition.Y + ViewportSize.Y * 0.5, WorldPosition, WorldDirection))
	{
		// Calculate the swing location using the swing plane and deprojected click location
		const FVector SwingLocation = FMath::LinePlaneIntersection(WorldPosition, WorldPosition + WorldDirection, SwingPlane);
		const FVector NewSwingToBall = SwingLocation - ActivePitch->GetActorLocation();
		SwingSphere->SetWorldLocation(SwingLocation);
		if (NewSwingToBall.SizeSquared() < SwingToBall.SizeSquared()) {
			SwingToBall = NewSwingToBall;
			BestSwingLocation = SwingLocation;
			BestBallLocation = ActivePitch->GetActorLocation();
		}
	}
}

void ACatchingControllerBase::OnSwingFinished()
{
	bIsSwinging = false;
	ReticleSensitivity *= 10;
	if (!ActivePitch || ActivePitch->GetStatus() != EBallStatus::BS_Hit)
	{
		// THIS IS CHEATING ( * 2 )
		DrawDebugSphere(GetWorld(), BestSwingLocation, SwingSphereRadius * 2, 13, FColor::Blue, false, 2);
		DrawDebugSphere(GetWorld(), BestBallLocation, 3.9, 13, FColor::Red, false, 2);
	}
	else
	{
		DrawDebugSphere(GetWorld(), BestSwingLocation, SwingSphereRadius * 2, 15, FColor::Purple, false, 2, 0);
	}
	SwingSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
}


void ACatchingControllerBase::OnPitchThrown(APitchBase* Pitch)
{
	Super::OnPitchThrown(Pitch);
	// Balances difficulty
	SwingDuration = -0.0016 * (Pitch->SpeedMPH - 85) + BaseSwingDuration;
}


void ACatchingControllerBase::DeactivatePitch()
{
	Super::DeactivatePitch();
	/*Overlay->UpdatePitch(ActivePitch->PitchType, ActivePitch->SpeedMPH);
	switch (ActivePitch->Status)
	{
	case EBallStatus::BS_Ball:
		Overlay->UpdateCount(false);
		Overlay->UpdateSwing(false);
		Overlay->UpdateStrike(false); break;
	case EBallStatus::BS_Strike:
		Overlay->UpdateCount(true);
		if (!Overlay->SwingText->GetText().EqualTo(FText::FromString("Swung"))) {
			Overlay->UpdateSwing(false);
		}
		Overlay->UpdateStrike(true); break;
	case EBallStatus::BS_Hit:
		break;
	}*/
	ActivePitch->bExitCalled = true;
	ActivePitch = nullptr;
	bCanSwing = true;

}
