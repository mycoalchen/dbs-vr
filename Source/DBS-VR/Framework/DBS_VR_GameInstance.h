// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "DBS_VR_GameInstance.generated.h"

UENUM(BlueprintType)
enum class ETrainingMode : uint8
{
	None			UMETA(DisplayName = "None"),
	Zone			UMETA(DisplayName = "Zone"),
	Catching		UMETA(DisplayName = "Catching")
};

UCLASS()
class DBS_VR_API UDBS_VR_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		ETrainingMode TrainingMode = ETrainingMode::Zone;
	
};
