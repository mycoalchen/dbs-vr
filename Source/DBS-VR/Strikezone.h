// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Strikezone.generated.h"

UCLASS()
class DBS_VR_API AStrikezone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStrikezone();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* OverlapBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float StrikezoneTop = 1.082; // height, in meters, of top of strikezone box
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float StrikezoneBottom = 0.53; // height, in meters, of bottom of strikezone box
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float StrikezoneWidth = 43.18; // width, in meters, of home plate
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Suggested Location")
		FVector SuggestedLocation; // Location to be placed at based on Top, Bottom, and Width
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the overlapbox is overlapped
	UFUNCTION()
	virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	virtual void PostInitializeComponents() override;
};
