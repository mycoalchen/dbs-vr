// Fill out your copyright notice in the Description page of Project Settings.


#include "PrecisionOverlay.h"
#include "Components/TextBlock.h"
#include "../Pitches/PitchBase.h"

void UPrecisionOverlay::NativeConstruct()
{
	for (UTextBlock* Block : {StrikeText, LocationText, PitchText, HitText, MissText, ResultText, SwingText})
	{
		Block->SetText(FText::FromString(FString("")));
	}
	CountText->SetText(FText::FromString(FString("0-0")));
}

#pragma region Count and result
void UPrecisionOverlay::SetResult(int result)
{
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearResult"));
	switch (result)
	{
	case 1:
		ResultText->SetText(FText::FromString("Struck out")); break;
	case 2:
		ResultText->SetText(FText::FromString("Walked")); break;
	case 3:
		ResultText->SetText(FText::FromString("Hit")); break;
	default:
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, TEXT("SetResult result param error in PrecisionOverlay"));
	}
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, ResultDisplayTime, false);
}

void UPrecisionOverlay::ClearResult()
{
	Strikes = Balls = 0;
	ResultText->SetText(FText::FromString(""));
	CountText->SetText(FText::FromString("0-0"));
}

void UPrecisionOverlay::UpdateCount(bool Strike)
{
	Strikes += Strike;
	Balls += !Strike;
	TArray< FStringFormatArg > args;
	args.Add(FStringFormatArg(Balls));
	args.Add(FStringFormatArg(Strikes));
	CountText->SetText(FText::FromString(FString::Format(TEXT("{0}-{1}"), args)));
	if (Strikes == 3)
		SetResult(1);
	else if (Balls == 4)
		SetResult(2);
}
#pragma endregion

void UPrecisionOverlay::UpdatePitch(EPitchType Type, int SpeedMPH)
{
	TArray<FStringFormatArg> args;
	args.Add(FStringFormatArg(SpeedMPH));
	FString PitchType;
	switch (Type) {
	case EPitchType::PT_Fast4: PitchType = FString("4-Seam Fastball"); break;
	case EPitchType::PT_Fast2: PitchType = FString("2-Seam Fastball"); break;
	case EPitchType::PT_Cut: PitchType = FString("Cutter"); break;
	case EPitchType::PT_Curve: PitchType = FString("Curveball"); break;
	}
	args.Add(FStringFormatArg(PitchType));
	PitchText->SetText(FText::FromString(FString::Format(TEXT("{1}\r{0} mph"), args)));
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearPitch"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, PitchDisplayTime, false);
}

void UPrecisionOverlay::ClearPitch()
{
	PitchText->SetText(FText::FromString(""));
}

void UPrecisionOverlay::UpdateStrike(bool Strike)
{
	if (Strike) StrikeText->SetText(FText::FromString("Strike"));
	else StrikeText->SetText(FText::FromString("Ball"));
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearStrike"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, StrikeDisplayTime, false);
}

void UPrecisionOverlay::ClearStrike()
{
	StrikeText->SetText(FText::FromString(""));
}

void UPrecisionOverlay::UpdateHit(bool Hit)
{
	if (Hit) {
		HitText->SetText(FText::FromString("Hit"));
		SetResult(3);
	}
	else HitText->SetText(FText::FromString("Missed"));
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearHit"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, HitDisplayTime, false);
}

void UPrecisionOverlay::ClearHit()
{
	HitText->SetText(FText::FromString(""));
}

// v is the shortest vector from the swing sphere to the ball over the duration of the swing
void UPrecisionOverlay::UpdateMiss(FVector v, float m)
{
	FString s = FString();
	if (v.Z > m)
		s = "Low";
	if (v.Z < -m)
		s = "High";
	if (v.Y > m)
	{
		if (!s.IsEmpty()) s += "/Left";
		else s = "Left";
	}
	if (v.Y < -m)
	{
		if (!s.IsEmpty()) s += "/Right";
		else s = "Right";
	}
	if (v.X > m)
	{
		if (!s.IsEmpty()) s += "/Early";
		else s = "Early";
	}
	if (v.X < -m)
	{
		if (!s.IsEmpty()) s += "/Late";
		else s = "Late";
	}
	MissText->SetText(FText::FromString(s));
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearMiss"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, HitDisplayTime, false);
}

void UPrecisionOverlay::ClearMiss()
{
	MissText->SetText(FText::FromString(""));
}

void UPrecisionOverlay::UpdateSwing(bool Swung)
{
	if (Swung) SwingText->SetText(FText::FromString("Swung"));
	else SwingText->SetText(FText::FromString("Taken"));
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ClearSwing"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, ResultDisplayTime, false);
}

void UPrecisionOverlay::ClearSwing()
{
	SwingText->SetText(FText::FromString(""));
}
