// Copyright Epic Games, Inc. All Rights Reserved.

#include "DBS_VR.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DBS_VR, "DBS_VR" );
