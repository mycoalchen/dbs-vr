// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CatcherWall.generated.h"

UCLASS()
class DBS_VR_API ACatcherWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACatcherWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnBoxHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	
	void OnCatchBall(class APitchBase* Pitch);
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Boxes")
		class UBoxComponent* Front;
	// These slanted boxes prevent the ball from being caught offscreen
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Boxes")
		class UBoxComponent* Top;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Boxes")
		class UBoxComponent* Bottom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Boxes")
		class UBoxComponent* Right;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Boxes")
		class UBoxComponent* Left;
	
};
