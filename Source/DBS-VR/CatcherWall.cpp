// Fill out your copyright notice in the Description page of Project Settings.


#include "CatcherWall.h"
#include "Components/BoxComponent.h"
#include "Pitches/PitchBase.h"

// Sets default values
ACatcherWall::ACatcherWall()
{
	PrimaryActorTick.bCanEverTick = false;
	
	Front = CreateDefaultSubobject<UBoxComponent>(FName("Front"));
	SetRootComponent(Front);

	Top = CreateDefaultSubobject<UBoxComponent>(FName("Top"));
	Bottom = CreateDefaultSubobject<UBoxComponent>(FName("Bottom"));
	Right = CreateDefaultSubobject<UBoxComponent>(FName("Right"));
	Left = CreateDefaultSubobject<UBoxComponent>(FName("Left"));

	for (UBoxComponent* Box : {Front, Top, Bottom, Right, Left})
	{
		Box->SetCollisionResponseToAllChannels(ECR_Block);
		Box->SetBoxExtent(FVector(10, 150, 150));
		Box->OnComponentHit.AddDynamic(this, &ACatcherWall::OnBoxHit);
		if (Box != Front)
		{
			Box->SetupAttachment(Front);
		}
	}

	Top->SetRelativeLocation(FVector(-25, 0, 70));
	Top->SetRelativeRotation(FRotator(-69, 0, 0));
	Bottom->SetRelativeLocation(FVector(-25, 0, -70));
	Bottom->SetRelativeRotation(FRotator(69, 0, 0));
	Right->SetRelativeLocation(FVector(-2, 92, 0));
	Right->SetRelativeRotation(FRotator(0, -52.5, 0));
	Left->SetRelativeLocation(FVector(-2, -92, 0));
	Left->SetRelativeRotation(FRotator(0, 52.5, 0));

}

// Called when the game starts or when spawned
void ACatcherWall::BeginPlay()
{
	Super::BeginPlay();
	
	for (UBoxComponent* Box : { Front, Top, Bottom, Right, Left })
	{
		Box->OnComponentHit.AddDynamic(this, &ACatcherWall::OnBoxHit);
	}
}

void ACatcherWall::OnBoxHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	APitchBase* Pitch = Cast<APitchBase>(OtherActor);
	if (Pitch) OnCatchBall(Pitch);
}

void ACatcherWall::OnCatchBall(APitchBase* Pitch)
{
	if (Pitch->GetStatus() != EBallStatus::BS_Hit)
	{
		Pitch->PMC->SetVelocityInLocalSpace(FVector(0, 0, 0));
		Pitch->SpinRotator = FRotator(0, 0, 0);
		Pitch->DestroyBlurMeshes();
		Pitch->SetLifeSpan(1);
	}
}
