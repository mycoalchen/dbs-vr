// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

UENUM(Blueprintable, BlueprintType)
enum class EPitchType : uint8
{
	PT_Fast4	UMETA(DisplayName = "4-Seam Fastball"),
	PT_Fast2	UMETA(DisplayName = "2-Seam Fastball"),
	PT_Cut		UMETA(DisplayName = "Cutter"),
	PT_Curve	UMETA(DisplayName = "Curveball"),
};

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Pitcher.generated.h"

UCLASS()
class DBS_VR_API APitcher : public APawn
{
	GENERATED_BODY()
	
public:	
	APitcher();

	virtual void Tick(float DeltaTime) override;

	// Training Controller in the world
	UPROPERTY(BlueprintReadWrite)
		class ATrainingControllerBase* TrainingController;
	
	// Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		class USkeletalMeshComponent* Body_Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Capsule")
		class UCapsuleComponent* Capsule;
	
	// Point to throw ball from
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		class USceneComponent* ReleasePoint;
	// Target to throw ball at (hard-coded gravity/break adjustment)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch target")
		class USceneComponent* PitchTarget;
	UPROPERTY(BlueprintReadWrite, Category = "Pitch target")
		bool DrawPitchTarget = false;
	UPROPERTY(BlueprintReadWrite, Category = "Pitching")
		bool bCanPitch = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pitch types")
		TSubclassOf<class APitchBase> Fastball4Class;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pitch types")
		TSubclassOf<class APitchBase> CurveballClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pitch types")
		TSubclassOf<class APitchBase> Fastball2Class;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pitch types")
		TSubclassOf<class APitchBase> CutterClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		FRotator Fastball4StartRotator = FRotator(0, 90, 0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		FRotator CurveballStartRotator = FRotator(-31, -60, -19);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		FRotator Fastball2StartRotator = FRotator(0, 0, 0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		FRotator CutterStartRotator = FRotator(-30, 90, 0);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float Fastball4MaxMph = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float Fastball4MinMph = 95;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float Fastball2MaxMph = 98;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float Fastball2MinMph = 92;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float CurveballMaxMph = 85;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float CurveballMinMph = 80;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float CutterMaxMph = 96;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float CutterMinMph = 90;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		TArray<float> MphBounds = { 95, 100, 92, 98, 90, 96, 80, 85 };

	UFUNCTION(BlueprintCallable, Exec, Category = "Pitch functions")
		void ThrowRandomPitch();
	UFUNCTION(BlueprintCallable, Exec, Category = "Pitch functions")
		void BeginThrowPitch(EPitchType PitchType, float MPH, float SpinRate);


	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Animation")
		void PlayPitchAnimation();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		float ReleaseDelay = 0.7; // Time after animation starts to release pitch

	// Called when we're ready to throw a new pitch
	UFUNCTION()
		void DeactivatePitch();
	
protected:
	virtual void BeginPlay() override;

private:
	// Called when the timer in BeginThrowPitch finishes (so the ball is thrown at the correct point in the animation)
	UFUNCTION()
		void ThrowPitch(EPitchType PitchType, float MPH, float SpinRate);

	
};
